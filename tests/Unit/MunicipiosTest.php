<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;
use App\Models\Municipios;

class MunicipiosTest extends TestCase {

    protected function setUp() : void {
        parent::setUp();
        $faker = Faker::create();
        $this->name = $faker->name;
        $this->email = $faker->unique()->safeEmail;
        $this->password = 'password';
        $user = new User([
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password
        ]);
        $user->save();
    }

    public function testCreateMunicipio(){
        $payload = [
            'token' => $this->getToken(),
            'municipio' => Str::random(14)
        ];
        $response = $this->post('/api/municipio', $payload);
        $response->assertStatus(201);   // HTTP 201 Created
    }

    public function testGetMunicipios(){
        $response = $this->get('/api/municipios');
        $response->assertStatus(200);
    }

    public function testUpdateMunicipio(){
        $municipio = new Municipio([
            'name' => Str::random(14)
        ]);
        $municipio->save();
        $payload = [
            'token' => $this->getToken(),
            'id' => $municipio->id,
            'municipio' => Str::random(14)
        ];
        $response = $this->put('/api/municipio', $payload);
        $response->assertStatus(201);   // HTTP 201 Created
    }

    public function testDeleteMunicipio(){
        $municipio = new Municipio([
            'name' => Str::random(14)
        ]);
        $municipio->save();
        $payload = [
            'token' => $this->getToken(),
            'id' => $municipio->id,
        ];
        $response = $this->delete('/api/municipio', $payload);
        $response->assertStatus(200);
    }

    private function getToken() : String {
        $credenciales = [
            'email' => $this->email,
            'password' => $this->password
        ];
        $response = $this->post('/api/auth/login', $credenciales);
        $data = $response->decodeResponseJson();
        return $data['token'];
    }
}
