<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reportes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ruta');
            $table->integer('numero_eco');
            $table->string('sitio');
            $table->boolean('anonimo')->default(false);
            $table->longText('descripcion')->nullable(false);
            $table->unsignedInteger('user_id')->nullable(false);
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('municipio_id')->nullable(false);
            $table->unsignedInteger('type_id')->nullable(false);
            $table->unsignedInteger('inspector_id');
            $table->unsignedInteger('supervisor_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('municipio_id')->references('id')->on('municipios');
            $table->foreign('type_id')->references('id')->on('types');
            $table->foreign('inspector_id')->references('id')->on('users');
            $table->foreign('supervisor_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reportes');
    }
}
