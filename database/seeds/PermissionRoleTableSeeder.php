<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

/**
 * Class PermissionRoleTableSeeder.
 */
class PermissionRoleTableSeeder extends Seeder
{

    /**
     * Run the database seed.
     */
    public function run()
    {
        // Create Roles
        Role::create(['name' => config('access.users.visor_role')]);
        Role::create(['name' => config('access.users.gestor_role')]);
        Role::create(['name' => config('access.users.inspector_role')]);
       // Role::create(['name' => config('access.users.default_user')]);

        // // Create Permissions
        // Permission::create(['name' => 'update_users']);
        // Permission::create(['name' => 'create_reports']);

    }
}
