<?php


namespace App\Api\V1\Controllers;


use App\Api\V1\Requests\TypeRequest;
use App\Http\Controllers\Controller;
use App\Repositories\TypeRepository;



class TypeController extends Controller
{
    /**
     * @var ListsRepository
     */
    protected $TypeRepository;

    /**
     * UserController constructor.
     *
     * @param ListsRepository $listsRepository
     */
    public function __construct(TypeRepository $TypeRepository)
    {
        $this->TypeRepository = $TypeRepository;
    }

    public function createType(TypeRequest $request)
    {
        $Type = $this->TypeRepository->createType($request->all());
        $permissions = Auth::guard()->user()->getAllPermissions();
        return response()->json([
            'status' => 'ok',
            'type' => $Type,
            'permissions' => $permissions
        ], 201);
    }

    public function getType()
    {
        $type = $this->TypeRepository->getType();

        return $type;
    }

    public function updateType(TypeRequest $request)
    {
        $Type = $this->TypeRepository->updateType($request->all());
        return response()->json([
            'status' => 'ok',
            'type' => $Type
        ], 201);
    }
    public function deleteType(TypeRequest $request)
    {
        $Type = $this->TypeRepository->deleteType($request->all());
        return response()->json([
            'status' => 'ok',
            'message' => 'Eliminado correctamente'
        ], 201);
    }
}
