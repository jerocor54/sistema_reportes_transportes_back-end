<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

class CustomersRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => ['required'],
            'email' => ['email'],
            'phone' => ['min:10|numeric'],
            'age' => ['numeric'],

        ];
    }

    public function authorize()
    {
        return $this->user()->hasPermissionTo('create_sub');
    }
}
