<?php

namespace App\Providers;

use Dingo\Api\Dispatcher;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Models\Bitacora;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        
       
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //Created Report
        Event::listen('eloquent.created: App\Models\Reportes', function($report){
            $bitacora = new Bitacora;
            $bitacora->actividad = 'Registro';
            $bitacora->user_id = auth()->user()->id;
            $bitacora->previous_user_id = 7;
            $bitacora->reporte_id = $report->id;

            $bitacora->save();
        });

        //Updated Report
        Event::listen('eloquent.updated: App\Models\Reportes', function($report){
            $bitacora = new Bitacora;
            $bitacora->actividad = 'Actualización';
            $bitacora->user_id = auth()->user()->id;
            $bitacora->previous_user_id = 7;
            $bitacora->reporte_id = $report->id;

            $bitacora->save();
        });
    }
}
